//jQuery.sap.includeScript("../js/signature_pad_min.js");
jQuery.sap.require("com.sap.build.standard.toptoyCandC.js.signature_pad_min");

sap.ui.define(["com/sap/build/standard/toptoyCandC/controller/BaseController",
	"./utilities",
	"sap/ui/core/routing/History",
	"sap/m/MessageBox"
], function (BaseController, Utilities, History, MessageBox) {
	"use strict";

	return BaseController.extend("com.sap.build.standard.toptoyCandC.controller.CancelOrder", {
		onInit: function () {
			this.mBindingOptions = {};
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getTarget("CancelOrder").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));

			var oModel = new sap.ui.model.json.JSONModel();
			this.getView().setModel(oModel, "HandleOutModel");

		},

		handleRouteMatched: function () {
			// ensure button is selected
			var cancelButton = this.getView().byId("cancelButton");
			this.getView().byId("handOutGoodsSegmentedButton").setSelectedButton(cancelButton);

			// clear search
			this.getView().byId("searchfieldCancel").setValue("");

			this.createCancelModel();
		},

		createCancelModel: function () {
			var cancelModel = this.getView().getModel("CancelModel");
			if (!cancelModel) {
				cancelModel = new sap.ui.model.json.JSONModel();
				cancelModel.setDefaultBindingMode(sap.ui.model.BindingMode.TwoWay);
			}
			cancelModel.setData({
				OrdersToBeOverdueView: true,
				IsNotExpressOrder: false,
				IsExpressOrder: false,
				Order: {},
				ReturnedItems: [],
				OrderItems: [],
				ItemsToBePosted: [],
				ItemsInOrder: 0,
				ReturnShipping: false,
			});
			this.getView().setModel(cancelModel, "CancelModel");
		},

		showOrdersToBeOverdueView: function () {
			var cancelModel = this.getView().getModel("CancelModel");
			cancelModel.getData().OrdersToBeOverdueView = true;
			cancelModel.getData().IsNotExpressOrder = false;
			cancelModel.getData().IsExpressOrder = false;
			cancelModel.refresh();
		},

		showIsNotExpressOrder: function () {
			var cancelModel = this.getView().getModel("CancelModel");
			cancelModel.getData().OrdersToBeOverdueView = false;
			cancelModel.getData().IsNotExpressOrder = true;
			cancelModel.getData().IsExpressOrder = false;

			// ensure date is removed
			cancelModel.getData().ReturnedItems = [];
			cancelModel.getData().ReturnShipping = false;
			cancelModel.refresh();
		},

		showIsExpressOrder: function () {
			var cancelModel = this.getView().getModel("CancelModel");
			cancelModel.getData().OrdersToBeOverdueView = false;
			cancelModel.getData().IsNotExpressOrder = false;
			cancelModel.getData().IsExpressOrder = true;
			cancelModel.refresh();
		},

		isReturnedItemsVisible: function (items) {
			return items.length > 0 ? true : false;
		},

		isTrackIdVisible: function (trackId) {
			return trackId !== undefined && trackId !== "" ? true : false;
		},

		countItems: function (items) {
			return items.length;
		},

		onSearch: function (oEvent) {
			var query = oEvent.getParameter("query");

			if (query.length === 0) {
				this.showOrdersToBeOverdueView();
				return;
			}

			this.searchOrderIdAndTrackAndTrace(query);

			document.activeElement.blur();
		},

		onScan: function (oEvent) {
			var query = oEvent.getParameter("newValue");

			if (query.length === 0) {
				this.showOrdersToBeOverdueView();
				return;
			}

			this.searchOrderIdAndTrackAndTrace(query);

			document.activeElement.blur();
		},

		searchOrderIdAndTrackAndTrace: function (query) {
			this.getView().getModel().read("/CancelOrderSet", {
				filters: [new sap.ui.model.Filter("Search", sap.ui.model.FilterOperator.EQ, query)],
				success: function (oData) {
					var cancelModel = this.getView().getModel("CancelModel");
					cancelModel.getData().Order = oData.results[0];
					cancelModel.refresh(true);

					this.getOrderItems(cancelModel.getData().Order.SalesOrderId).then(function () {
						// update view
						if (cancelModel.getData().Order.IsExpressOrder) {
							this.showIsExpressOrder();
						} else {
							this.showIsNotExpressOrder();
						}

					}.bind(this), function (error) {
						// error handling
						this.errorCallBackShowInPopUp(error);
					}.bind(this));

				}.bind(this),
				error: function (error) {
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			});
		},

		getOrderItems: function (orderId) {
			return new Promise(function (resolve, reject) {
				this.getView().getModel().read("/CancelOrderSet('" + orderId + "')/Items", {
					success: function (oData) {
						var cancelModel = this.getView().getModel("CancelModel");
						cancelModel.getData().OrderItems = oData.results;

						// count number of items in order
						cancelModel.getData().ItemsInOrder = 0;
						cancelModel.getData().OrderItems.forEach(function (item) {
							var intQuantity = parseInt(item.Quantity, 10);
							cancelModel.getData().ItemsInOrder = cancelModel.getData().ItemsInOrder + intQuantity;
						}.bind(this));
						cancelModel.refresh(true);
						resolve();
					}.bind(this),
					error: function (error) {
						reject(error);
					}
				});
			}.bind(this));
		},

		addReturnItem: function (oEvent) {
			var query = oEvent.getParameter("query");
			if (query === "" || query.length < 1) {
				return;
			}

			this.isItemPartOfOrder(query).then(function (scannedItem) {
				var cancelModel = this.getView().getModel("CancelModel");
				var returnItems = cancelModel.getData().ReturnedItems;
				var scannedItemQuanityInt = parseInt(scannedItem.Quantity, 10);
				var equalEanNumber = 0;

				// check if item exists in returnItems array
				for (var i = 0; i < returnItems.length; i++) {
					if (returnItems[i].Ean === scannedItem.Ean) {
						equalEanNumber++;
					}
				}

				// if item exists
				if (equalEanNumber > 0) {
					if (equalEanNumber < scannedItemQuanityInt) {
						// add new item since it might have a different resale status
						cancelModel.getData().ReturnedItems.push({
							Ean: scannedItem.Ean,
							MaterialId: scannedItem.MaterialId,
							MaterialDesc: scannedItem.MaterialDesc,
							Quantity: "1.000",
							IsResale: true
						});
						cancelModel.refresh(true);
					} else {
						this.clearScanItem();
						var orderId = this.getView().getModel("CancelModel").getData().Order.SalesOrderId;
						MessageBox.error("The quantity of scanned item exceeds the item type in order: " + orderId);
						return;
					}
				} else {
					// add item since it is not part of returnItems array
					cancelModel.getData().ReturnedItems.push({
						Ean: scannedItem.Ean,
						MaterialId: scannedItem.MaterialId,
						MaterialDesc: scannedItem.MaterialDesc,
						SalesOrderId: scannedItem.SalesOrderId,
						Quantity: "1.000",
						IsResale: true
					});
					cancelModel.refresh(true);
				}

				this.clearScanItem();

				// focus field again after scan
				this.setScanFocus();

				// return shipping if all items are returned
				this.setReturnShipping();

			}.bind(this), function () {
				this.clearScanItem();

				// ean number not found in order items
				var orderId = this.getView().getModel("CancelModel").getData().Order.SalesOrderId;
				MessageBox.error("Item is not part of order: " + orderId);
			}.bind(this));
		},

		setScanFocus: function () {
			var searchField = this.getView().byId('scanItemInput');
			searchField.setValue("");
			setTimeout(function () {
				searchField.focus();
			}, 400);
		},

		clearScanItem: function () {
			this.getView().byId("scanItemInput").setValue("");

			// hide keyboard
			document.activeElement.blur();
		},

		isItemPartOfOrder: function (ean) {
			return new Promise(function (resolve, reject) {
				var cancelModel = this.getView().getModel("CancelModel");
				var orderItems = cancelModel.getData().OrderItems;
				for (var i = 0; i < orderItems.length; i++) {
					if (orderItems[i].Ean === ean) {
						resolve(orderItems[i]);
						break;
					}
				}
				reject();
			}.bind(this));
		},

		deleteReturnItem: function (oEvent) {
			var cancelModel = this.getView().getModel("CancelModel");
			var oitem = oEvent.getSource().getParent().getBindingContext("CancelModel").getObject();
			var array = cancelModel.getData().ReturnedItems;
			array.splice(array.indexOf(oitem), 1);
			cancelModel.refresh(true);

			// return shipping if all items are returned
			this.setReturnShipping();
		},

		setReturnShipping: function () {
			var cancelModel = this.getView().getModel("CancelModel");
			if (cancelModel.getData().ReturnedItems.length >= cancelModel.getData().ItemsInOrder) {
				cancelModel.getData().ReturnShipping = true;
			} else {
				cancelModel.getData().ReturnShipping = false;
			}
			cancelModel.refresh(true);
		},

		onHandOutButtonItemPress: function (oEvent) {
			var oBindingContext = oEvent.getSource().getBindingContext();

			return new Promise(function (fnResolve) {
				this.doNavigate("Handleoutgoods", oBindingContext, fnResolve, "");
			}.bind(this)).catch(function (err) {
				if (err !== undefined) {
					var dialog = new sap.m.Dialog({
						title: 'Error',
						icon: "sap-icon://message-error",
						type: 'Message',
						content: new sap.m.Text({
							text: err.message
						}),
						beginButton: new sap.m.Button({
							text: 'OK',
							press: function () {
								dialog.close();
							}.bind(this)
						}),
						afterClose: function () {
							dialog.destroy();
						}
					});

					dialog.open();

					//MessageBox.error(err.message);
				}
			});
		},

		onReceiveButtonItemPress: function (oEvent) {
			var oBindingContext = oEvent.getSource().getBindingContext();

			return new Promise(function (fnResolve) {
				this.doNavigate("Receivegoods", oBindingContext, fnResolve, "");
			}.bind(this)).catch(function (err) {
				if (err !== undefined) {
					var dialog = new sap.m.Dialog({
						title: 'Error',
						icon: "sap-icon://message-error",
						type: 'Message',
						content: new sap.m.Text({
							text: err.message
						}),
						beginButton: new sap.m.Button({
							text: 'OK',
							press: function () {
								dialog.close();
							}.bind(this)
						}),
						afterClose: function () {
							dialog.destroy();
						}
					});

					dialog.open();

					//MessageBox.error(err.message);
				}
			});
		},

		doNavigate: function (sRouteName, oBindingContext, fnPromiseResolve, sViaRelation) {
			var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oModel = (oBindingContext) ? oBindingContext.getModel() : null;

			var sEntityNameSet;
			if (sPath !== null && sPath !== "") {
				if (sPath.substring(0, 1) === "/") {
					sPath = sPath.substring(1);
				}
				sEntityNameSet = sPath.split("(")[0];
			}
			var sNavigationPropertyName;
			var sMasterContext = this.sMasterContext ? this.sMasterContext : sPath;

			if (sEntityNameSet !== null) {
				sNavigationPropertyName = sViaRelation || this.getOwnerComponent().getNavigationPropertyForNavigationWithContext(sEntityNameSet,
					sRouteName);
			}
			if (sNavigationPropertyName !== null && sNavigationPropertyName !== undefined) {
				if (sNavigationPropertyName === "") {
					this.oRouter.navTo(sRouteName, {
						context: sPath,
						masterContext: sMasterContext
					}, true);
				} else {
					oModel.createBindingContext(sNavigationPropertyName, oBindingContext, null, function (bindingContext) {
						if (bindingContext) {
							sPath = bindingContext.getPath();
							if (sPath.substring(0, 1) === "/") {
								sPath = sPath.substring(1);
							}
						} else {
							sPath = "undefined";
						}

						// If the navigation is a 1-n, sPath would be "undefined" as this is not supported in Build
						if (sPath === "undefined") {
							this.oRouter.navTo(sRouteName, {}, true);
						} else {
							this.oRouter.navTo(sRouteName, {
								context: sPath,
								masterContext: sMasterContext
							}, true);
						}
					}.bind(this));
				}
			} else {
				this.oRouter.navTo(sRouteName, {}, true);
			}

			if (typeof fnPromiseResolve === "function") {
				fnPromiseResolve();
			}
		},

		createItemsToBePosted: function () {
			return new Promise(function (resolve, reject) {
				var cancelModel = this.getView().getModel("CancelModel");
				
				// empty items to be posted
				cancelModel.getData().ItemsToBePosted = [];
				
				var returnedItems = cancelModel.getData().ReturnedItems;
				var itemsToBePosted = cancelModel.getData().ItemsToBePosted;
				// run through all scanned items
				for (var i = 0; i < returnedItems.length; i++) {
					var returnedItem = returnedItems[i];
					var itemExitsInItemsToBePosted = false;
					var foundItem;
					
					// check if item already added to itemsToBePosted
					for (var y = 0; y < itemsToBePosted.length; y++) {
						if (returnedItem.MaterialId === itemsToBePosted[y].MaterialId) {
							itemExitsInItemsToBePosted = true;
							foundItem = itemsToBePosted[y]; 
						}
					}

					if (!itemExitsInItemsToBePosted) {
						// add item to ItemsToBePosted
						var item = {
							"SalesOrderId": returnedItem.SalesOrderId, //"100000488",
							"MaterialId": returnedItem.MaterialId, //"100150001",
							"QuantityResale": returnedItem.IsResale ? "1" : "0",
							"QuantityScrap": returnedItem.IsResale ? "0" : "1"
						};
						itemsToBePosted.push(item);
					} else { // update existing item in itemsToBePosted
						// if resale then update QuantityResale
						if (returnedItem.IsResale) {
							var intQuantityResale = parseInt(foundItem.QuantityResale, 10);
							var newValue = intQuantityResale + 1;
							foundItem.QuantityResale = "" + newValue + "";
						} else {
							var intQuantityScrap = parseInt(foundItem.QuantityScrap, 10);
							var newValue = intQuantityScrap + 1;
							foundItem.QuantityScrap = "" + newValue + "";
						}
					}
				cancelModel.refresh();
				}
				resolve();
			}.bind(this));
		},

		approveReturnItems: function () {
			this.createItemsToBePosted().then(function () {
				var cancelModel = this.getView().getModel("CancelModel");
				var itemsToBePosted = cancelModel.getData().ItemsToBePosted;
				var data = {
					SalesOrderId: cancelModel.getData().Order.SalesOrderId,
					DoCancel: true,
					ReimburseFreight: cancelModel.getData().ReturnShipping,
					ReturnItems: itemsToBePosted
				};
				
				this.getView().getModel().create("/CancelOrderSet", data, {
					success: function (oData, response) {
						// set success message
						var msg = "Order is now cancelled.";
						for (var i = 0; i < itemsToBePosted.length; i++) {
							if (!itemsToBePosted[i].QuantityScrap !== "0" || !itemsToBePosted[i].QuantityScrap > 0) {
								msg = "Order is now cancelled. \n\n Not resale articles must be adjusted from block stock.";
								break;
							}

						}

						MessageBox.success(msg, {
							onClose: function () {
								this.clearCancelProces();
							}.bind(this)
						});
					}.bind(this),
					error: function (error) {
						this.errorCallBackShowInPopUp(error);
					}.bind(this)
				});
			}.bind(this));
		},

		cancelOrder: function () {
			var dataUpdate = {
				DoCancel: true
			};
			var orderId = this.getView().getModel("CancelModel").getData().Order.SalesOrderId;
			this.getView().getModel().update("/CancelOrderSet('" + orderId + "')", dataUpdate, {
				success: function (oData, response) {
					var msg = "The order is now cancelled. \n\n Item is now ready to resale.";
					MessageBox.success(msg, {
						onClose: function () {
							this.clearCancelProces();
						}.bind(this)
					});
				}.bind(this),
				error: function (error) {
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			});
		},

		clearCancelProces: function () {
			this.createCancelModel();
			// clear search
			this.getView().byId("searchfieldCancel").setValue("");
		}

	});
}, /* bExport= */ true);