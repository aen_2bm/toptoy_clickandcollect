//jQuery.sap.includeScript("../js/signature_pad_min.js");
jQuery.sap.require("com.sap.build.standard.toptoyCandC.js.signature_pad_min");

sap.ui.define(["sap/ui/core/mvc/Controller",
	"./utilities",
	"sap/ui/core/routing/History"
], function(BaseController, Utilities, History) {
	"use strict";

	return BaseController.extend("com.sap.build.standard.toptoyCandC.controller.Handleoutgoods", {
		onInit: function() {
			this.mBindingOptions = {};
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getTarget("Handleoutgoods").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));

			var oModel = new sap.ui.model.json.JSONModel();
			this.getView().setModel(oModel, "HandleOutModel");
			this.resetViewModel();

			//Give focus to search field
			/*	var oInput = this.getView().byId("searchfieldHandleOut");
				oInput.addEventDelegate({
					onAfterRendering: function() {
						oInput._inputElement.focus();
					},
					onfocusin: function() {
						//Select text
					}
				});*/
		},

		handleRouteMatched: function(oEvent) {
			var oParams = {};
			if (oEvent.mParameters.data.context) {
				this.sContext = oEvent.mParameters.data.context;
				var oPath;
				if (this.sContext) {
					oPath = {
						path: "/" + this.sContext,
						parameters: oParams
					};
					this.getView().bindObject(oPath);
				}
			}

			this.resetViewModel();

			var handOutButton = this.getView().byId("handOutButton");
			this.getView().byId("handOutGoodsSegmentedButton").setSelectedButton(handOutButton);
		},

		resetViewModel: function() {
			var viewModel = this.getView().getModel("HandleOutModel");
			this.getView().byId("idVerificationSwitch").setState(false);
			viewModel.setData({
				TrackIDFound: false,
				TrackId: "",
				DeliveryDocID: "",
				OrderId: "",
				ToBePickedupBy: "",
				LongtextOld: "",
				LongtextNew: "",
				IsExpressOrder: false,
				ExchangeStickers: [],
				IsReserved: false,
				SalesInfo: ""
			});

			viewModel.refresh();
		},

		onSearch: function(arg) {
			// if(arg.getParameters().clearButtonPressed){
			// 	this.getView().byId("searchfieldHandleOut").focus();
			// 	return;
			// }

			var searchText = this.getView().byId("searchfieldHandleOut").mProperties.value;
			//var searchText= oEvent.mParameters.query;
			this.resetViewModel();
			if (searchText) {
				this.getView().byId("searchfieldHandleOut").setBusy(true);
				var onDataReceived = {
					success: function(oData, oResponse) {
						this.getView().byId("searchfieldHandleOut").setBusy(false);
						if (oData.TrackId) {
							//Present material data
							this.getView().getModel("HandleOutModel").getData().TrackIDFound = true;
							this.getView().getModel("HandleOutModel").getData().TrackId = oData.TrackId;
							this.getView().getModel("HandleOutModel").getData().DeliveryDocID = oData.DeliveryDocID;
							this.getView().getModel("HandleOutModel").getData().OrderId = oData.OrderId;
							this.getView().getModel("HandleOutModel").getData().IsExpressOrder = oData.IsExpressOrder;
							this.getView().getModel("HandleOutModel").getData().ToBePickedupBy = oData.ToBePickedupBy;
							this.getView().getModel("HandleOutModel").getData().LongtextOld = oData.LongTextOld;
							this.getView().getModel("HandleOutModel").getData().LongtextNew = "";
							this.getView().getModel("HandleOutModel").getData().IsReserved = oData.IsReserved;
							this.getView().getModel("HandleOutModel").getData().SalesInfo = oData.SalesInfo;

							this.getView().getModel("HandleOutModel").refresh();
							document.activeElement.blur();
						} else {
							sap.m.MessageToast.show("Track and Trace or Order Id is not found ");
						}

					}.bind(this),
					error: function(error) {
						this.getView().byId("searchfieldHandleOut").setBusy(false);
						this.errorCallBackShowInPopUp(error);
					}.bind(this)
				};
				this.getView().getModel().read("/HandoutSet('" + searchText + "')", onDataReceived);
			}

		},

		onReceiveButtonItemPress: function(oEvent) {
			var oBindingContext = oEvent.getSource().getBindingContext();

			return new Promise(function(fnResolve) {
				this.doNavigate("Receivegoods", oBindingContext, fnResolve, "");
			}.bind(this)).catch(function(err) {
				if (err !== undefined) {
					var dialog = new sap.m.Dialog({
						title: 'Error',
						icon: "sap-icon://message-error",
						type: 'Message',
						content: new sap.m.Text({
							text: err.message
						}),
						beginButton: new sap.m.Button({
							text: 'OK',
							press: function() {
								dialog.close();
							}.bind(this)
						}),
						afterClose: function() {
							dialog.destroy();
						}
					});

					dialog.open();

					//MessageBox.error(err.message);
				}
			});
		},
		
		onCancelButtonItemPress: function(oEvent) {
			this.oRouter.navTo("CancelOrder", {}, true);
		},

		doNavigate: function(sRouteName, oBindingContext, fnPromiseResolve, sViaRelation) {
			var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oModel = (oBindingContext) ? oBindingContext.getModel() : null;

			var sEntityNameSet;
			if (sPath !== null && sPath !== "") {
				if (sPath.substring(0, 1) === "/") {
					sPath = sPath.substring(1);
				}
				sEntityNameSet = sPath.split("(")[0];
			}
			var sNavigationPropertyName;
			var sMasterContext = this.sMasterContext ? this.sMasterContext : sPath;

			if (sEntityNameSet !== null) {
				sNavigationPropertyName = sViaRelation || this.getOwnerComponent().getNavigationPropertyForNavigationWithContext(sEntityNameSet,
					sRouteName);
			}
			if (sNavigationPropertyName !== null && sNavigationPropertyName !== undefined) {
				if (sNavigationPropertyName === "") {
					this.oRouter.navTo(sRouteName, {
						context: sPath,
						masterContext: sMasterContext
					}, true);
				} else {
					oModel.createBindingContext(sNavigationPropertyName, oBindingContext, null, function(bindingContext) {
						if (bindingContext) {
							sPath = bindingContext.getPath();
							if (sPath.substring(0, 1) === "/") {
								sPath = sPath.substring(1);
							}
						} else {
							sPath = "undefined";
						}

						// If the navigation is a 1-n, sPath would be "undefined" as this is not supported in Build
						if (sPath === "undefined") {
							this.oRouter.navTo(sRouteName, {}, true);
						} else {
							this.oRouter.navTo(sRouteName, {
								context: sPath,
								masterContext: sMasterContext
							}, true);
						}
					}.bind(this));
				}
			} else {
				this.oRouter.navTo(sRouteName, {}, true);
			}

			if (typeof fnPromiseResolve === "function") {
				fnPromiseResolve();
			}
		},

		onExchangeStickersButtonPress: function(oEvent) {
			if (!this.exchangeStickersPopup) {
				this.getOwnerComponent().runAsOwner(function() {
					this.exchangeStickersPopup = sap.ui.xmlfragment("com.sap.build.standard.toptoyCandC.view.ExchangeStickerPopover", this);
					this.getView().addDependent(this.exchangeStickersPopup);
				}.bind(this));
			}

			this.exchangeStickersPopup.open();
		},

		onCloseExchangeStickerPopup: function(oEvent) {
			this.exchangeStickersPopup.close();
		},

		onDeleteExchangeSticker: function(oEvent) {
			var place = oEvent.getParameters().listItem.getBindingContext("HandleOutModel").getPath().length - 1;
			var itemNumber = oEvent.getParameters().listItem.getBindingContext("HandleOutModel").getPath().slice(place);

			var oModel = this.getView().getModel("HandleOutModel");
			oModel.oData.ExchangeStickers.splice(itemNumber, 1);
			oModel.setData(oModel.oData);
			oModel.refresh(true);
		},

		onExchangeStickerScanned: function(oEvent) {
			var object = {
				"Number": oEvent.getParameters().value
			};

			var oModel = this.getView().getModel("HandleOutModel");
			oModel.oData.ExchangeStickers.push(object);
			oModel.setData(oModel.oData);
			oModel.refresh(true);

			//Reset input field
			oEvent.getSource().setValue("");
		},

		onDeliverButtonPress: function(oEvent) {

			var oModel = this.getView().getModel("HandleOutModel");

			if (!oModel.getData().IsReserved) {

				//Check if ID verification has been performed
				var idVerificationSwitch = this.getView().byId("idVerificationSwitch");
				if (idVerificationSwitch.getState() === false) {
					var dialog = new sap.m.Dialog({
						title: "Verify ID",
						type: 'Message',
						content: new sap.m.Text({
							text: "Please do ID verification"
						}),
						beginButton: new sap.m.Button({
							text: 'OK',
							press: function() {
								dialog.close();
							}.bind(this)
						}),
						afterClose: function() {
							dialog.destroy();
						}
					});

					dialog.open();

					return;
				}
			} else if (oModel.getData().IsReserved) {
				//Check if ID verification has been performed
				var idCustomerPaidSwitch = this.getView().byId("idCustomerPaidSwitch");
				if (idCustomerPaidSwitch.getState() === false) {
					var dialog = new sap.m.Dialog({
						title: "Reservation",
						type: 'Message',
						content: new sap.m.Text({
							text: "Customer must pay in store"
						}),
						beginButton: new sap.m.Button({
							text: 'OK',
							press: function() {
								dialog.close();
							}.bind(this)
						}),
						afterClose: function() {
							dialog.destroy();
						}
					});

					dialog.open();

					return;
				}
			}
			//Check if Exchange Stickers has been entered in case of Expressdelivery
			else if (this.getView().getModel("HandleOutModel").getData().IsExpressOrder && oModel.oData.ExchangeStickers.length === 0) {
				var dialog = new sap.m.Dialog({
					title: "Scan Exchange Stickers",
					type: 'Message',
					content: new sap.m.Text({
						text: "You did not scan exchange stickers"
					}),
					beginButton: new sap.m.Button({
						text: 'Continue',
						press: function() {
							dialog.close();
							this.continueToDelivery(oEvent);
							return;
						}.bind(this)
					}),
					endButton: new sap.m.Button({
						text: 'Cancel',
						press: function() {
							dialog.close();
						}.bind(this)
					}),
					afterClose: function() {
						dialog.destroy();
					}
				});

				dialog.open();
			}
			// else {
			// 	if (!oModel.getData().IsReserved) {
			// 		//User must sign for pickup
			// 		this.showSignatureScreen(oEvent);
			// 	} else {
			// 		//User has to pay in store and does not have to sign
			// 		this.postGoodsDelivery(null);
			// 	}
			// }

			this.continueToDelivery(oEvent);
		},

		continueToDelivery: function(oEvent) {
			if (!this.getView().getModel("HandleOutModel").getData().IsReserved) {
				//User must sign for pickup
				this.showSignatureScreen(oEvent);
			} else {
				this.getView().setBusy(true);
				//User has to pay in store and does not have to sign
				this.postGoodsDelivery(null);
			}
		},

		showSignatureScreen: function(oEvent) {
			if (!this.signaturePopup) {
				this.getOwnerComponent().runAsOwner(function() {
					this.signaturePopup = sap.ui.xmlfragment("com.sap.build.standard.toptoyCandC.view.SignaturePopover", this);
					this.getView().addDependent(this.signaturePopup);
				}.bind(this));
			}

			return new Promise(function(fnResolve) {
				this.signaturePopup.attachEventOnce("afterOpen", null, fnResolve);
				this.signaturePopup.openBy(oEvent.getSource());
				if (this.signaturePopup) {
					this.signaturePopup.attachAfterOpen(function() {

						var canvas = document.getElementById('signature-pad');

						//Make writeable

						var ctx = canvas.getContext('2d');
						ctx.globalCompositeOperation = 'source-over'; // default value

						var ratio = Math.max(window.devicePixelRatio || 1, 1);

						canvas.width = window.innerWidth; //canvas.offsetWidth * ratio;
						canvas.height = window.innerHeight; //canvas.offsetHeight * ratio;

						if (window.innerHeight < window.innerWidth) {
							//Landscape
							canvas.height = canvas.height - 48 - 48 - 4;

						} else {
							//Portrait
							canvas.height = canvas.height - 48 - 48 - 4;
						}

						this.signaturePad = new SignaturePad(canvas, {
							backgroundColor: 'rgb(255, 255, 255)' // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
						});
					}.bind(this));
				} else {
					this.signaturePopup = this.signaturePopup.getParent();
				}
			}.bind(this)).catch(function(err) {
				if (err !== undefined) {
					var dialog = new sap.m.Dialog({
						title: 'Error',
						icon: "sap-icon://message-error",
						type: 'Message',
						content: new sap.m.Text({
							text: err.message
						}),
						beginButton: new sap.m.Button({
							text: 'OK',
							press: function() {
								dialog.close();
							}.bind(this)
						}),
						afterClose: function() {
							dialog.destroy();
						}
					});

					dialog.open();
					//MessageBox.error(err.message);
				}
			});
		},

		saveSignature: function(oEvent) {

			if (this.signaturePad.isEmpty()) {
				var dialog = new sap.m.Dialog({
					title: "No signature",
					type: 'Message',
					content: new sap.m.Text({
						text: "Please sign"
					}),
					beginButton: new sap.m.Button({
						text: 'OK',
						press: function() {
							dialog.close();
						}.bind(this)
					}),
					afterClose: function() {
						dialog.destroy();
					}
				});

				dialog.open();

				return;
			}

			this.signaturePopup.setBusy(true);

			this.uploadSignature();
		},

		uploadSignature: function() {
			var base64ImagePng = this.signaturePad.toDataURL();

			var imageWithoutBase64Prefix = base64ImagePng.replace("data:image/png;base64,", "");

			this.postGoodsDelivery(imageWithoutBase64Prefix);
		},

		postGoodsDelivery: function(signImageData) {

			var parameters = {
				success: function(oData, response) {
					this.clearView();
					sap.m.MessageToast.show("Goods delivery complete");
				}.bind(this),
				error: function(oError) {
					this.getView().setBusy(false);
					this.signaturePopup.setBusy(false);
					this.errorCallBackShowInPopUp(oError);
				}.bind(this)
			};

			var dataCreate = {};
			if (this.getView().getModel("HandleOutModel").getData().IsExpressOrder) {

				//get Exchange Stickers
				var postExchangeStickerString = "";
				var array = this.getView().getModel("HandleOutModel").getData().ExchangeStickers;
				for (var i = 0; i < array.length; i++) {
					if (postExchangeStickerString !== "") {
						postExchangeStickerString += ";";
					}
					postExchangeStickerString += array[i].Number;
				}

				//Request for upload on Express Sales Order
				dataCreate.OrderId = this.getView().getModel("HandleOutModel").getData().OrderId;
				dataCreate.IsExpressOrder = true;
				dataCreate.ExchangeStickerInfo = postExchangeStickerString;

				if (!this.getView().getModel("HandleOutModel").getData().IsReserved) {
					dataCreate.SignaturePicture = signImageData;
				}
			} else {
				//Request for upload on Delivery
				dataCreate.DeliveryDocID = this.getView().getModel("HandleOutModel").getData().DeliveryDocID;

				if (!this.getView().getModel("HandleOutModel").getData().IsReserved) {
					dataCreate.SignaturePicture = signImageData;
				}
			}

			var trackId = this.getView().getModel("HandleOutModel").getData().TrackId;
			var path = "/HandoutSet('" + trackId + "')";
			this.getView().getModel().update(path, dataCreate, parameters);
		},

		closeSignature: function(oEvent) {
			// Clears the canvas
			this.signaturePad.clear();

			this.signaturePopup.close();
		},

		clearView: function() {
			this.getView().byId("searchfieldHandleOut").setValue("");
			this.getView().byId("searchfieldHandleOut").focus();
			this.getView().byId("idVerificationSwitch").setState(false);
			this.getView().byId("idCustomerPaidSwitch").setState(false);
			this.getView().setBusy(false);
			if (this.signaturePopup) {
				this.signaturePopup.setBusy(false);
				this.closeSignature();
			}
			this.resetViewModel();
		},

		formatExchangeStickerString: function(fullString) {
			var returnString = "";

			if (fullString) {
				var arrayLength = fullString.length;
				for (var i = 0; i < arrayLength; i++) {
					if (returnString !== "") {
						returnString += "\r\n";
					}
					returnString += fullString[i].Number;

					//Do something
				}
			}

			return returnString;
		},

		countExchangeStickers: function(fullString) {
			var returnText = "Exchange Stickers : ";

			if (fullString) {
				return returnText + fullString.length;
			}
			return returnText;
		},

		errorCallBackShowInPopUp: function(oError) {

			if (oError) {
				if (oError.responseText) {
					var errorCode = JSON.parse(oError.responseText).error.code;
					var errorMessage = JSON.parse(oError.responseText).error.message.value;

					//sap.m.MessageBox.error("Error: " + errorCode + " - " + errorMessage);

					var dialog = new sap.m.Dialog({
						title: 'Error',
						icon: "sap-icon://message-error",
						type: 'Message',
						content: new sap.m.Text({
							text: errorCode + " - " + errorMessage
						}),
						beginButton: new sap.m.Button({
							text: 'OK',
							press: function() {
								dialog.close();
							}.bind(this)
						}),
						afterClose: function() {
							dialog.destroy();
						}
					});

					dialog.open();
				} else {
					var dialog = new sap.m.Dialog({
						title: 'Error',
						icon: "sap-icon://message-error",
						type: 'Message',
						content: new sap.m.Text({
							text: oError.message
						}),
						beginButton: new sap.m.Button({
							text: 'OK',
							press: function() {
								dialog.close();
							}.bind(this)
						}),
						afterClose: function() {
							dialog.destroy();
						}
					});

					dialog.open();

					//sap.m.MessageBox.error("Error: " + oError.message);
				}
			}
		},

		getDeliverButtonText: function(isReserved) {
			if (isReserved) {
				return "Deliver";
			}

			return "Sign & Deliver";
		},
		
		isExchangeStickersVisible : function(isExpressOrder, isReserved){
			if(isReserved){
				return false;
			}
			
			if(isExpressOrder){
				return true;
			}
			
			return false;
		}
	});
}, /* bExport= */ true);