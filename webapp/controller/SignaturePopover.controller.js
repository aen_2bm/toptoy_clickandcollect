// sap.ui.define(["sap/ui/core/mvc/Controller",
// 	"sap/m/MessageBox",
// 	"./utilities",
// 	"sap/ui/core/routing/History"
// ], function(BaseController, MessageBox, Utilities, History) {
// 	"use strict";

// 	return BaseController.extend("com.sap.build.standard.toptoyCandC.controller.SignaturePopover", {
// 		onInit: function() {
// 			this.mBindingOptions = {};
// 			this._oDialog = this.getView().getContent()[0];
// 		},

// 		resizeCanvas: function() {
// 			// When zoomed out to less than 100%, for some very strange reason,
// 			// some browsers report devicePixelRatio as less than 1
// 			// and only part of the canvas is cleared then.
// 			var canvas = document.getElementById('signature-pad');
// 			var ratio = Math.max(window.devicePixelRatio || 1, 1);
// 			canvas.width = canvas.offsetWidth * ratio;
// 			canvas.height = canvas.offsetHeight * ratio;
// 			canvas.getContext("2d").scale(ratio, ratio);
// 		},

// 		saveSignature: function(oEvent) {
// 			this.closeSignature();
// 			this._oDialog.close();
// 			this.getView().setBusy(true);
// 			var canvas = document.getElementById('signature-pad');
			
// 			sap.m.MessageToast.show("Goods delivery complete");
// 		},

// 		closeSignature: function(oEvent) {
// 			var canvas = document.getElementById('signature-pad');
			
// 			var signaturePad = new SignaturePad(canvas);
// 			// Clears the canvas
// 			signaturePad.clear();
			
// 			this._oDialog.close();
// 		},

// 		onExit: function() {
// 			this._oDialog.destroy();
// 		}
// 	});
// }, /* bExport= */ true);