sap.ui.define(["sap/ui/core/mvc/Controller",
	"./utilities",
	"sap/ui/core/routing/History",
	"sap/m/MessageBox"
], function(BaseController, Utilities, History, MessageBox) {
	"use strict";

	return BaseController.extend("com.sap.build.standard.toptoyCandC.controller.Receivegoods", {
		onInit: function() {

			var oModel = new sap.ui.model.json.JSONModel();
			this.getView().setModel(oModel, "ReceiveModel");
			this.resetViewModel();

			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getTarget("Receivegoods").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));

			var that = this;
			this.getView().byId("searchfieldReceive").addEventDelegate({
				onAfterRendering: function() {
					that.getView().byId("searchfieldReceive").focus();
					that.getView().byId("searchfieldReceive").focus();
					$("searchfieldReceive").focus();
				},
				onfocusin: function() {
					//Select all text
				}
			});
		},

		handleRouteMatched: function(oEvent) {
			var oParams = {};
			if (oEvent.mParameters.data.context) {
				this.sContext = oEvent.mParameters.data.context;
				var oPath;
				if (this.sContext) {
					oPath = {
						path: "/" + this.sContext,
						parameters: oParams
					};
					this.getView().bindObject(oPath);
				}
			}

			this.resetViewModel();
			var receiveButton = this.getView().byId("ReceivButton");
			this.getView().byId("receiveSegmentedButton").setSelectedButton(receiveButton);
		},

		resetViewModel: function() {
			var viewModel = this.getView().getModel("ReceiveModel");
			// this.getView().byId("idNotifyCustomerSwitch").setState(false);
			viewModel.setData({
				TrackIDFound: false,
				TrackId: "",
				DeliveryDocID: "",
				ToBePickedupBy: "",
				NotifyCustomer: false,
				RelatedTrackIds: "",
				TrackIdsCount: "",
				LongtextOld: "",
				LongtextNew: "",
				NotifyCustomerState: false
			});

			viewModel.refresh();

		},

		onSearch: function(oEvent) {
			this.resetViewModel();
			var searchText = oEvent.getParameter("query");
			if (searchText) {
				this.getView().byId("searchfieldReceive").setBusy(true);
				var onDataReceived = {
					success: function(oData, oResponse) {
						this.getView().byId("searchfieldReceive").setBusy(false);
						if (oData.TrackId) {
							//Check if the delivery has already been received
							var textSplit = oData.LongtextOld.split(("\r\n"));
							var found = false;
							for (var i = 0; i < textSplit.length; ++i) {
								var element = textSplit[i];
								if (element.indexOf(searchText) !== -1) {
									if (!found) {
										found = true;
										var str = element;
										str = str.replace(searchText, "");

										var popupMessage = "Delivery has already been received. Lokation" + str;

										var dialog = new sap.m.Dialog({
											title: "Already received",
											type: 'Message',
											content: new sap.m.Text({
												text: popupMessage
											}),
											beginButton: new sap.m.Button({
												text: 'OK',
												press: function() {
													dialog.close();
												}.bind(this)
											}),
											afterClose: function() {
												dialog.destroy();
											}
										});

										dialog.open();

										//Show popup
										// var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
										// sap.m.MessageBox.show(popupMessage, {
										// 	icon: MessageBox.Icon.NONE,
										// 	title: "Already received",
										// 	actions: [MessageBox.Action.OK],
										// 	defaultAction: MessageBox.Action.OK,
										// 	styleClass: bCompact ? "sapUiSizeCompact" : ""
										// });
										return;
									} else {
										return;
									}
								}
							}
							
							// show warning if sales order belongs to other shop
							if(oData.WarningMessage !== "") {
								MessageBox.warning(oData.WarningMessage);
							}

							var trackIDsCount = this.getTrackIdsCount(oData.RelatedTrackIds);
							
							//Present material data
							this.getView().getModel("ReceiveModel").getData().TrackIDFound = true;
							this.getView().getModel("ReceiveModel").getData().TrackId = oData.TrackId;
							this.getView().getModel("ReceiveModel").getData().DeliveryDocID = oData.DeliveryDocID;
							this.getView().getModel("ReceiveModel").getData().ToBePickedupBy = oData.ToBePickedupBy;
							this.getView().getModel("ReceiveModel").getData().RelatedTrackIds = oData.RelatedTrackIds;
							this.getView().getModel("ReceiveModel").getData().LongtextOld = oData.LongtextOld;
							this.getView().getModel("ReceiveModel").getData().TrackIdsCount = trackIDsCount;

							this.getView().getModel("ReceiveModel").getData().LongtextNew = this.setDefLocation(oData.LongtextOld);

							var longTextOldCount = this.getNumberOfReceivedOrders(oData.LongtextOld);
							if (trackIDsCount - longTextOldCount <= 1) {
								this.getView().getModel("ReceiveModel").getData().NotifyCustomer = true;
							} else {
								this.getView().getModel("ReceiveModel").getData().NotifyCustomer = false;
							}

							this.getView().getModel("ReceiveModel").refresh();
							document.activeElement.blur();
						} else {
							sap.m.MessageToast.show("Track and Trace number not found:" + searchText);
						}
					}.bind(this),
					error: function(error) {
						this.getView().byId("searchfieldReceive").setBusy(false);
						this.errorCallBackShowInPopUp(error);
					}.bind(this)
				};
				this.getView().getModel().read("/ReceivalSet('" + searchText + "')", onDataReceived);
			}
		},
		
		getTrackIdsCount : function(relatedTrackIds){
			if(relatedTrackIds){
				return relatedTrackIds.split("-").length + 1; //add one as the current ID we selected should be considered also
			}
			else{
				return 1; //add one as the current ID we selected should be considered also
			}
		},
		
		getNumberOfReceivedOrders : function(longText){
			if(longText){
				return longText.split(("\r\n")).length;
			}
			else{
				return 0;
			}
		},

		setDefLocation: function(longtext) {
			var longTextSplit = longtext.split("\r\n");
			var length = longTextSplit.length;
			var lastLine = longTextSplit[length - 1];
			var lastlocation = lastLine.split(": ")[1];

			return lastlocation;
		},

		onHandOutButtonItemPress: function(oEvent) {
			var oBindingContext = oEvent.getSource().getBindingContext();

			return new Promise(function(fnResolve) {
				this.doNavigate("Handleoutgoods", oBindingContext, fnResolve, "");
			}.bind(this)).catch(function(err) {
				if (err !== undefined) {
					var dialog = new sap.m.Dialog({
						title: 'Error',
						icon: "sap-icon://message-error",
						type: 'Message',
						content: new sap.m.Text({
							text:  err.message
						}),
						beginButton: new sap.m.Button({
							text: 'OK',
							press: function() {
								dialog.close();
							}.bind(this)
						}),
						afterClose: function() {
							dialog.destroy();
						}
					});

					dialog.open();
					
					
					//MessageBox.error(err.message);
				}
			});
		},
		
		onCancelButtonItemPress: function(oEvent) {
			this.oRouter.navTo("CancelOrder", {}, true);
		},

		submitData: function(oEvent) {

			if (!this.getView().getModel("ReceiveModel").getData().LongtextNew) {
				var dialog = new sap.m.Dialog({
					title: "Location",
					type: 'Message',
					content: new sap.m.Text({
						text: "Please enter location"
					}),
					beginButton: new sap.m.Button({
						text: 'OK',
						press: function() {
							dialog.close();
						}.bind(this)
					}),
					afterClose: function() {
						dialog.destroy();
					}
				});

				dialog.open();

				// var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
				// sap.m.MessageBox.show("Please enter location", {
				// 	icon: MessageBox.Icon.NONE,
				// 	title: "Location",
				// 	actions: [MessageBox.Action.OK],
				// 	defaultAction: MessageBox.Action.OK,
				// 	styleClass: bCompact ? "sapUiSizeCompact" : ""
				// });
				return;
			}

			var parameters = {
				success: function(oData, oResponse) {
					sap.m.MessageToast.show("Delivery data updated");
					this.resetViewModel();
					this.getView().byId("searchfieldReceive").setValue("");
					this.getView().byId("searchfieldReceive").focus();
				}.bind(this),
				error: function(error) {
					this.errorCallBackShowInPopUp(error);
				}.bind(this)
			};

			var data = {
				"LongtextNew": this.getView().getModel("ReceiveModel").getData().LongtextNew,
				"NotifyCustomer": this.getView().getModel("ReceiveModel").getData().NotifyCustomer
			};

			this.getView().getModel().update("/ReceivalSet('" + this.getView().getModel("ReceiveModel").getData().TrackId + "')", data,
				parameters);
		},

		doNavigate: function(sRouteName, oBindingContext, fnPromiseResolve, sViaRelation) {

			var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oModel = (oBindingContext) ? oBindingContext.getModel() : null;

			var sEntityNameSet;
			if (sPath !== null && sPath !== "") {
				if (sPath.substring(0, 1) === "/") {
					sPath = sPath.substring(1);
				}
				sEntityNameSet = sPath.split("(")[0];
			}
			var sNavigationPropertyName;
			var sMasterContext = this.sMasterContext ? this.sMasterContext : sPath;

			if (sEntityNameSet !== null) {
				sNavigationPropertyName = sViaRelation || this.getOwnerComponent().getNavigationPropertyForNavigationWithContext(sEntityNameSet,
					sRouteName);
			}
			if (sNavigationPropertyName !== null && sNavigationPropertyName !== undefined) {
				if (sNavigationPropertyName === "") {
					this.oRouter.navTo(sRouteName, {
						context: sPath,
						masterContext: sMasterContext
					}, true);
				} else {
					oModel.createBindingContext(sNavigationPropertyName, oBindingContext, null, function(bindingContext) {
						if (bindingContext) {
							sPath = bindingContext.getPath();
							if (sPath.substring(0, 1) === "/") {
								sPath = sPath.substring(1);
							}
						} else {
							sPath = "undefined";
						}

						// If the navigation is a 1-n, sPath would be "undefined" as this is not supported in Build
						if (sPath === "undefined") {
							this.oRouter.navTo(sRouteName, {}, true);
						} else {
							this.oRouter.navTo(sRouteName, {
								context: sPath,
								masterContext: sMasterContext
							}, true);
						}
					}.bind(this));
				}
			} else {
				this.oRouter.navTo(sRouteName, {}, true);
			}

			if (typeof fnPromiseResolve === "function") {
				fnPromiseResolve();
			}
		},

		errorCallBackShowInPopUp: function(oError) {

			if (oError) {
				if (oError.responseText) {
					var errorCode = JSON.parse(oError.responseText).error.code;
					var errorMessage = JSON.parse(oError.responseText).error.message.value;
					
						var dialog = new sap.m.Dialog({
						title: 'Error',
						icon: "sap-icon://message-error",
						type: 'Message',
						content: new sap.m.Text({
							text:  errorCode + " - " + errorMessage
						}),
						beginButton: new sap.m.Button({
							text: 'OK',
							press: function() {
								dialog.close();
							}.bind(this)
						}),
						afterClose: function() {
							dialog.destroy();
						}
					});

					dialog.open();
					

					//sap.m.MessageBox.error("Error: " + errorCode + " - " + errorMessage);
				} else {
					var dialog = new sap.m.Dialog({
						title: 'Error',
						icon: "sap-icon://message-error",
						type: 'Message',
						content: new sap.m.Text({
							text: oError.message
						}),
						beginButton: new sap.m.Button({
							text: 'OK',
							press: function() {
								dialog.close();
							}.bind(this)
						}),
						afterClose: function() {
							dialog.destroy();
						}
					});

					dialog.open();
				//	sap.m.MessageBox.error("Error: " + oError.message);
				}
			}
		}

	});
}, /* bExport= */ true);