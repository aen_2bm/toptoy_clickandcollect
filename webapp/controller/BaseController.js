/*global history */
sap.ui.define([
		"sap/ui/core/mvc/Controller",
		"sap/ui/core/routing/History"
	], function (Controller, History) {
		"use strict";

		return Controller.extend("com.sap.build.standard.toptoyCandC.controller.BaseController", {
			/**
			 * Convenience method for accessing the router in every controller of the application.
			 * @public
			 * @returns {sap.ui.core.routing.Router} the router for this component
			 */
			getRouter : function () {
				return this.getOwnerComponent().getRouter();
			},

			/**
			 * Convenience method for getting the view model by name in every controller of the application.
			 * @public
			 * @param {string} sName the model name
			 * @returns {sap.ui.model.Model} the model instance
			 */
			getModel : function (sName) {
				return this.getView().getModel(sName);
			},

			/**
			 * Convenience method for setting the view model in every controller of the application.
			 * @public
			 * @param {sap.ui.model.Model} oModel the model instance
			 * @param {string} sName the model name
			 * @returns {sap.ui.mvc.View} the view instance
			 */
			setModel : function (oModel, sName) {
				return this.getView().setModel(oModel, sName);
			},

			/**
			 * Convenience method for getting the resource bundle.
			 * @public
			 * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
			 */
			getResourceBundle : function () {
				return this.getOwnerComponent().getModel("i18n").getResourceBundle();
			},
			
			errorCallBackShowInPopUp: function(oError) {
				if (oError) {
					var errorMessage = "";
					var Message;
					if (oError.responseText) {
						if (oError.responseText.substr(0,1) === "<"){
							var xmlParser = new DOMParser();
							var xmlDoc = xmlParser.parseFromString(oError.responseText,"text/xml");
							errorMessage = xmlDoc.getElementsByTagName("body")[0].getElementsByTagName("h1")[0].childNodes[0].nodeValue;
						} else{
							errorMessage = JSON.parse(oError.responseText).error.message.value;
						}
						Message = errorMessage;
					} else if (oError.body) {
//						errorCode = JSON.parse(oError.body).error.code;
						errorMessage = JSON.parse(oError.body).error.message.value;
						Message = errorMessage;
					} else {
						Message = oError.message;
					}
					sap.m.MessageBox.error(Message);
				}
			}
		});

	}
);